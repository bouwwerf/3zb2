/*
	Some simple bot chat functions.
	It's pretty shitty but I don't like highly configurable mods.
*/

#include "g_local.h"

void Bot_Say(edict_t *who, char *msg)
{
	char outmsg[256];
	int i;
	char *p;
	edict_t *cl_ent;

	outmsg[0] = 0;

	if (*msg == '\"')
	{
		msg[strlen(msg) - 1] = 0;
		msg++;
	}

	for (p = outmsg; *msg && (p - outmsg) < sizeof(outmsg) - 2; msg++)
	{
		*p++ = *msg;
	}
	*p = 0;

	for (i = 0; i < maxclients->value; i++)
	{
		cl_ent = g_edicts + 1 + i;

		if (!cl_ent->inuse)
			continue;
		if (!cl_ent->client)
			continue;
		
		if (!G_EntIsBot(cl_ent))
		{
			gi.cprintf(cl_ent, PRINT_CHAT, "%s: %s\n", who->client->pers.netname, outmsg);
		}
	}
}

qboolean Bot_CanChat(edict_t *ent, int chance)
{
	if (!ent || !ent->client)
	{
		return false;
	}

	if (!G_EntIsBot(ent))
	{
		return false;
	}

	if (ent->client->zc.chattime > level.time)
	{
		return false;
	}
	
	if (chance < 0 || chance > 100)
	{
		chance = 65;
	}
	
	// Randomize chatting
	if ((rand() % 100) <= chance)
	{
		ent->client->zc.chattime = level.time + 16.0 + (8.0 * (rand() % 7));

		return true;
	}
	
	return false;
}

void Bot_ChatEnterTheGame(edict_t *ent)
{
	if (!ent || !ent->client)
	{
		return;
	}

	if (!Bot_CanChat(ent, 65))
	{
		return;
	}

	switch (rand() % 45)
	{
		case 0: default: Bot_Say(ent, "Who wants some?"); break;
		case 1: Bot_Say(ent, "Are we having fun yet?"); break;
		case 2: Bot_Say(ent, "Hi there!"); break;
		case 4: Bot_Say(ent, "Hello!"); break;
		case 5: Bot_Say(ent, "G'day mate."); break;
		case 6: Bot_Say(ent, "I love the smell of BFG blasts in the morning!"); break;
		case 7: Bot_Say(ent, "Konnichiwa!"); break;
		case 8: Bot_Say(ent, "Everyone aboard? Let's rock."); break;
		case 9: Bot_Say(ent, "Let's see what you can do."); break;
		case 10: Bot_Say(ent, "Let the show begin."); break;
		case 11: Bot_Say(ent, "Be fruitiful and frag a lot."); break;
		case 12: Bot_Say(ent, "'sup bro?"); break;
		case 13: Bot_Say(ent, "Yo!"); break;
		case 14: Bot_Say(ent, "Wassup!"); break;
		case 15: Bot_Say(ent, "Hey."); break;
		case 16: Bot_Say(ent, "Hi."); break;
		case 17: Bot_Say(ent, "Greetings"); break;
		case 18: Bot_Say(ent, "Eh."); break;
		case 19: Bot_Say(ent, "I'm back!"); break;
		case 20: Bot_Say(ent, "Oi!"); break;
		case 21: Bot_Say(ent, "Welcome aboard."); break;
		case 22: Bot_Say(ent, "Howdy."); break;
		case 23: Bot_Say(ent, "You better not suck."); break;
		case 24: Bot_Say(ent, "Wait... what color am I?"); break;
		case 25: Bot_Say(ent, "Yull!"); break;
		case 26: Bot_Say(ent, "What da deallie yo?"); break;
		case 27: Bot_Say(ent, "Let's get it on!"); break;
		case 28: Bot_Say(ent, "Who's going to lose to me today?"); break;
		case 29: Bot_Say(ent, "Anarchists of the world unite!"); break;
		case 30: Bot_Say(ent, "To the Bat Cave!"); break;
		case 31: Bot_Say(ent, "Anyone up for slapping the criminal element around?"); break;
		case 32: Bot_Say(ent, "Make 'em say 'ugggggh'!"); break;
		case 33: Bot_Say(ent, "Lets rock and roll!"); break;
		case 34: Bot_Say(ent, "Present!"); break;
		case 35: Bot_Say(ent, "Here!"); break;
		case 36: Bot_Say(ent, "Nice night for a furball!"); break;
		case 37: Bot_Say(ent, "Eh mon!"); break;
		case 38: Bot_Say(ent, "Crap, where's a gun!?"); break;
		case 39: Bot_Say(ent, "I'm feelin' saucy today."); break;
		case 40: Bot_Say(ent, "Kick it!"); break;
		case 41: Bot_Say(ent, "Hey lullo!"); break;
		case 42: Bot_Say(ent, va("Meh, I hate %s..", level.mapname)); break;
		case 43: Bot_Say(ent, va("Oh yeah! I love %s!", level.mapname)); break;
		case 44: Bot_Say(ent, "Let's jam!"); break;
	}
}

void Bot_ChatFrag(edict_t *killer, edict_t *killed)
{
	if (!killer || !killer->client)
	{
		return;
	}

	if (!killed || !killed->client)
	{
		return;
	}

	if (!Bot_CanChat(killer, 80))
	{
		return;
	}

	switch (rand() % 60)
	{
		case 0: default: Bot_Say(killer, "Ain't nobody stoppin me!"); break;
		case 1: Bot_Say(killer, "Thank you, come again"); break;
		case 2: Bot_Say(killer, "Get the hell out of my way!"); break;
		case 3: Bot_Say(killer, "Nothing can stop me now"); break;
		case 4: Bot_Say(killer, "Ouch, that HAD to hurt!"); break;
		case 5: Bot_Say(killer, "Can anyone here play this game?"); break;
		case 6: Bot_Say(killer, "Waiter! Come clean up this mess!"); break;
		case 7: Bot_Say(killer, va("Need a band-aid %s?", killed->client->pers.netname)); break;
		case 8: Bot_Say(killer, va("Hey everyone, %s is a camping whore.", killed->client->pers.netname)); break;
		case 9: Bot_Say(killer, va("Sorry %s.  This is a NO BREATHING zone.", killed->client->pers.netname)); break;
		case 10: Bot_Say(killer, va("Damn %s, it should be illegal to suck so bad.", killed->client->pers.netname)); break;
		case 11: Bot_Say(killer, va("Excuse me while I urinate on your corpse, %s.", killed->client->pers.netname)); break;
		case 12: Bot_Say(killer, va("Hey %s... found your spleen.", killed->client->pers.netname)); break;
		case 13: Bot_Say(killer, va("Stick to Duke Nukem, %s.", killed->client->pers.netname)); break;
		case 14: Bot_Say(killer, va("Eww, I stepped on %s's severed ear!", killed->client->pers.netname)); break;
		case 15: Bot_Say(killer, va("You want some more %s?", killed->client->pers.netname)); break;
		case 16: Bot_Say(killer, va("Nice try %s... A for effort.", killed->client->pers.netname)); break;
		case 17: Bot_Say(killer, va("Just give up %s.", killed->client->pers.netname)); break;
		case 18: Bot_Say(killer, "How pathetic."); break;
		case 19: Bot_Say(killer, "Bahahaha!"); break;
		case 20: Bot_Say(killer, va("Your corpse made a fine screenshot %s.", killed->client->pers.netname)); break;
		case 21: Bot_Say(killer, va("Lovely chalk outline %s.", killed->client->pers.netname)); break;
		case 22: Bot_Say(killer, va("Next time %s... move.", killed->client->pers.netname)); break;
		case 23: Bot_Say(killer, va("Stick to observer mode %s.", killed->client->pers.netname)); break;
		case 24: Bot_Say(killer, va("Damn %s, you smell just like roast chicken.", killed->client->pers.netname)); break;
		case 25: Bot_Say(killer, "I love the sound of crunching bone. :)"); break;
		case 26: Bot_Say(killer, va("Dude, %s... your skin blows.", killed->client->pers.netname)); break;
		case 27: Bot_Say(killer, va("Hey everyone, %s wasn't wearing clean underwear!", killed->client->pers.netname)); break;
		case 28: Bot_Say(killer, va("Does your face hurt, %s? Well, its killin' me!", killed->client->pers.netname)); break;
		case 29: Bot_Say(killer, "Ooh, thats gotta hurt!"); break;
		case 30: Bot_Say(killer, "B-b-b-b-Bad to the bone!"); break;
		case 31: Bot_Say(killer, va("Oh my god, they killed %s! You basterds!", killed->client->pers.netname)); break;
		case 32: Bot_Say(killer, "I bet that hurt!"); break;
		case 33: Bot_Say(killer, va("%s's dead, Jim!", killed->client->pers.netname)); break;
		case 34: Bot_Say(killer, va("%s's loss is my gain!", killed->client->pers.netname)); break;
		case 35: Bot_Say(killer, "Game over!"); break;
		case 36: Bot_Say(killer, "Who's your daddy?"); break;
		case 37: Bot_Say(killer, "Die, scum!"); break;
		case 38: Bot_Say(killer, "Smells like burned wimp here!"); break;
		case 39: Bot_Say(killer, "You suck."); break;
		case 40: Bot_Say(killer, "You know what?... you suck!"); break;
		case 41: Bot_Say(killer, "Next time, duck..."); break;
		case 42: Bot_Say(killer, "Hasta la vista, baby!"); break;
		case 43: Bot_Say(killer, "I'm just a lean, mean, killing machine!"); break;
		case 44: Bot_Say(killer, "Mess with the best, die like the rest!"); break;
		case 45: Bot_Say(killer, "You suck didly-uck!"); break;
		case 46: Bot_Say(killer, va("You had guts, %s. Too bad they're scattered to kingdom come...", killed->client->pers.netname)); break;
		case 47: Bot_Say(killer, va("That'll teach you, %s!", killed->client->pers.netname)); break;
		case 48: Bot_Say(killer, va("I had to do it, %s. You understand.", killed->client->pers.netname)); break;
		case 49: Bot_Say(killer, va("It's nothing personal, %s.", killed->client->pers.netname)); break;
		case 50: Bot_Say(killer, va("You fly real nice, %s.", killed->client->pers.netname)); break;
		case 51: Bot_Say(killer, va("Oh, %s... what... what have I done?", killed->client->pers.netname)); break;
		case 52: Bot_Say(killer, va("Don't get so worked up, %s. It's just a game.", killed->client->pers.netname)); break;
		case 53: Bot_Say(killer, va("Keyboarder, %s?", killed->client->pers.netname)); break;
		case 54: Bot_Say(killer, va("I think %s is using a bot.", killed->client->pers.netname)); break;
		case 55: Bot_Say(killer, "Terminated."); break;
		case 56: Bot_Say(killer, va("You move like peanut butter, %s!", killed->client->pers.netname)); break;
		case 57: Bot_Say(killer, va("Nice pain WAVs %s.", killed->client->pers.netname)); break;
		case 58: Bot_Say(killer, va("%s is my domain! Bow!", level.mapname)); break;
		case 59: Bot_Say(killer, "BOW DOWN!"); break;
	}
}

void Bot_ChatQuadDamage(edict_t *ent)
{
	if (!ent)
	{
		return;
	}

	if (!ctf->value)
	{
		return;
	}

	if (!Bot_CanChat(ent, 95))
	{
		return;
	}

	switch (rand() % 6)
	{
		case 0: default: CTFSay_Team(ent, "I've got the Quad Damage!"); break;
		case 1: CTFSay_Team(ent, "OUR QUAD at %l!"); break;
		case 2: CTFSay_Team(ent, "[Quad Damage] %l"); break;
		case 3: CTFSay_Team(ent, "[QUAD] %l"); break;
		case 4: CTFSay_Team(ent, "I've got Quad at %l"); break;
		case 5: CTFSay_Team(ent, "Our Quad! [%l]"); break;
	}
}

void Bot_ChatPickupTech(edict_t *ent)
{
	if (!ent)
	{
		return;
	}

	if (!ctf->value)
	{
		return;
	}

	if (!Bot_CanChat(ent, 85))
	{
		return;
	}

	switch (rand() % 6)
	{
		case 0: default: CTFSay_Team(ent, "[%t] %l"); break;
		case 1: CTFSay_Team(ent, "Got %t at %l"); break;
		case 2: CTFSay_Team(ent, "Got %t [%h]"); break;
		case 3: CTFSay_Team(ent, "Got %t with %h!"); break;
		case 4: CTFSay_Team(ent, "%t at %l [%h]"); break;
		case 5: CTFSay_Team(ent, "I've picked up %t"); break;
	}
}

void Bot_ChatPickupFlag(edict_t *ent)
{
	if (!ent)
	{
		return;
	}

	if (!ctf->value)
	{
		return;
	}

	if (!Bot_CanChat(ent, 85))
	{
		return;
	}

	switch (rand() % 6)
	{
		case 0: default: CTFSay_Team(ent, "I've got the enemy flag!"); break;
		case 1: CTFSay_Team(ent, "Need escort at %l"); break;
		case 2: CTFSay_Team(ent, "Enemy flag in my possesion!"); break;
		case 3: CTFSay_Team(ent, "Need escort!"); break;
		case 4: CTFSay_Team(ent, "Got flag, need escort"); break;
		case 5: CTFSay_Team(ent, "Defend our flag carrier!"); break;
	}
}
